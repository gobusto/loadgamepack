/**
@file
@brief Any archive-related functions (other than loading/saving) live here.

Copyright (C) 2009-2015 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "package.h"

/*
[PUBLIC] Free a previously-allocated archive structure from memory.
*/

void pakFree(pak_s *pak)
{
  if (!pak) { return; }

  if (pak->entry)
  {
    long i;
    for (i = 0; i < pak->count; i++)
    {
      if (pak->entry[i]) { free(pak->entry[i]); }
    }
    free(pak->entry);
  }

  free(pak);
}

/*
[PUBLIC] Find an entry in a archive by name.
*/

pak_item_s *pakFind(const pak_s *pack, const char *name)
{
  long i = 0;

  if (!pack || !name || !pack->entry) { return NULL; }

  for (i = 0; i < pack->count; ++i)
  {
    if (pack->entry[i])
    {
      if (strcmp(pack->entry[i]->name, name) == 0) { return pack->entry[i]; }
    }
  }

  return NULL;
}

/*
[PUBLIC] Load the actual data for an archive entry into memory.
*/

pak_data_s *pakLoadData(const pak_s *pack, const pak_item_s *item)
{

  FILE       *file = NULL;
  pak_data_s *data = NULL;

  /* Ensure that the data can be loaded. */

  if      (!pack || !item) { return NULL; }
  else if (item->size < 1) { return NULL; }

  /* Open the archive file. */

  file = fopen (pack->path, "rb");

  if (!file) { return NULL; }

  /* Allocate memory for the data structure. */

  data = (pak_data_s*)malloc (sizeof(pak_data_s));

  if (data)
  {

    memset (data, 0, sizeof(pak_data_s));

    data->size = item->size;
    data->data = (unsigned char*)malloc (data->size);

    /* Load the data into memory (or clean up if memory allocation fails). */

    if (data->data)
    {

      fseek (file,          item->offs, SEEK_SET);
      fread (data->data, 1, item->size, file    );

    }
    else
    {

      pakFreeData (data);
      data = NULL;

    }

  }

  /* Close the archive file and return the result. */

  fclose (file);

  return data;

}

/*
[PUBLIC] Save the data loaded from an archive entry as a file.
*/

int pakSaveData(const pak_data_s *data, const char *file_name)
{
  FILE *fout = NULL;

  if (!file_name || !data || data->size < 1 || !data->data) { return -1; }

  fout = fopen(file_name, "wb");
  if (!fout) { return 1; }

  fwrite(data->data, 1, data->size, fout);
  fclose(fout);

  return 0;
}

/*
[PUBLIC] Free a previously-loaded data structure.
*/

void pakFreeData(pak_data_s *data)
{
  if (!data) { return; }
  if (data->data) { free(data->data); }
  free(data);
}
