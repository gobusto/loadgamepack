/**
@file
@brief Provides functions for extracting files from archives.

Copyright (C) 2009-2015 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __QQQ_ARCHIVE_H__
#define __QQQ_ARCHIVE_H__

/* Constants: */

#define PAK_MAXFILENAME 512 /**< @brief Maximum length of a file path.   */
#define PAK_MAXENTRYNAME 64 /**< @brief Maximum length of an entry name. */

/**
@brief Defines a list of supported archive types.
*/

typedef enum
{

  PAKTYPE_QPAK, /**< @brief Quake 1/2 PAK file. */
  PAKTYPE_DPAK, /**< @brief Daikatana PAK file. */
  PAKTYPE_IWAD, /**< @brief DOOM 1/2 IWAD file. */
  PAKTYPE_PWAD, /**< @brief DOOM 1/2 PWAD file. */
  PAKTYPE_WAD2, /**< @brief Quake 1 WAD2 file. */
  PAKTYPE_WAD3, /**< @brief Half-Life 1 WAD3 file. */
  PAKTYPE_MPC,  /**< @brief Hyperspace Delivery Boy MPC file. */
  PAKTYPE_GRP,  /**< @brief Build Engine GRP file. */
  PAKTYPE_HOG,  /**< @brief Descent HOG file. */
  PAKTYPE_CMP,  /**< @brief Duke Nukem 2 CMP file. */
  PAKTYPE_VPP1, /**< @brief Red Faction 1 .VPP archive format. */
  PAKTYPE_VPP2, /**< @brief Red Faction 2 .VPP archive format. */
  PAKTYPE_CSM,  /**< @brief Chasm: The Rift .BIN archive format. */
  PAKTYPE_RES,  /**< @brief System Shock 1 and 2 .RES archive format. */
  PAKTYPE_DARK, /**< @brief Dark Engine GAM/MIS DarkDB. */
  NUM_PAKTYPES  /**< @brief Total number of supported options. */

} pak_type_t;

/**
@brief Structure used for an individual pak entry.

Note that this structure doesn't contain any actual file data - it just defines
the size and position of an entry within an archive, so that it can be loaded.

@todo The name buffer should be dynamic, rather than assuming a fixed length.
*/

typedef struct
{

  char name[PAK_MAXENTRYNAME];  /**< @brief Name for this entry. */

  long offs;  /**< @brief Offset to data (in bytes). */
  long size;  /**< @brief Length of data (in bytes). */

} pak_item_s;

/**
@brief The main archive structure.

@todo The path buffer should be dynamic, rather than assuming a fixed length.
*/

typedef struct
{

  char path[PAK_MAXFILENAME]; /**< @brief Archive file name. */
  pak_type_t type;            /**< @brief Archive file type. */

  long count;         /**< @brief Number of stored files. */
  pak_item_s **entry; /**< @brief List of stored entries. */

} pak_s;

/**
@brief This structure simply descibes a raw binary data buffer.

It's used to store data when loading individual entries from archives.
*/

typedef struct
{

  long size;            /**< @brief Length of the raw data buffer, in bytes. */
  unsigned char *data;  /**< @brief Raw binary data loaded from the archive. */

} pak_data_s;

/**
@brief Attempt to load an archive file.

This function only loads the "table of contents" in order to avoid loading very
large archives into memory all at once. Individual items within the archive can
be loaded separately afterwards.

@param file_name The name/path of the file to be loaded.
@return A new archive structure on success, or NULL on failure.
*/

pak_s *pakLoad(const char *file_name);

/**
@brief Export an archive structure to a file, possibly in a different format.

For example, it's possible to load a VPP archive and export it as a Quake PAK.

@param pak The original archive structure to use as a source.
@param fmt The archive format of the output file.
@param file_name The name/path of the output file.
@return Zero on success, or non-zero on failure.
*/

int pakSave(const pak_s *pak, pak_type_t fmt, const char *file_name);

/**
@brief Free a previously-allocated archive structure from memory.

@param pak The archive structure to be deleted.
*/

void pakFree(pak_s *pak);

/**
@brief Find an entry in a archive by name.

If multiple entries have the same name, the first one found is returned.

@param pak The archive structure to be searched.
@param name The name of the entry to look for.
@return The matching entry on success, or NULL on failure.
*/

pak_item_s *pakFind(const pak_s *pak, const char *name);

/**
@brief Load the actual data for an archive entry into memory.

@todo We shouldn't need to provide both the archive AND the entry...

@param pak The archive file that cotains the entry.
@param item The archive entry to be loaded.
@return A new data structure on success, or NULL on failure.
*/

pak_data_s *pakLoadData(const pak_s *pak, const pak_item_s *item);

/**
@brief Save the data loaded from an archive entry as a file.

@param data The data structure to save.
@param file_name The name/path of the output file.
@return Zero on success, or non-zero on failure.
*/

int pakSaveData(const pak_data_s *data, const char *file_name);

/**
@brief Free a previously-loaded data structure.

@param data The data structure to be deleted.
*/

void pakFreeData(pak_data_s *data);

#endif /* __QQQ_ARCHIVE_H__ */
