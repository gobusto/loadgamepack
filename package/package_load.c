/**
@file
@brief Provides functions for importing archive structures from files.

Copyright (C) 2009-2015 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "package.h"

/* Quake/Quake II + Half-Life PAK-related constants: */

#define PACK_QPAK_4CC 0x4B434150  /**< @brief Spells "PACK" as ASCII text. */

#define PACK_QPAK_HEAD_SIZE 12  /**< @brief about */
#define PACK_QPAK_INFO_SIZE 64  /**< @brief about */
#define PACK_QPAK_NAME_SIZE 56  /**< @brief about */

/**
@brief Read a little-endian UINT16 value from a buffer.

@param x A pointer to the start of a binary data buffer.
@return The extracted value on success, or zero on failure.
*/

#define pakReadUI16_LE(x) ((x) ?\
  ((unsigned short)(x)[0]    ) +\
  ((unsigned short)(x)[1]<<8 ) : 0)

/**
@brief Read a little-endian UINT32 value from a buffer.

@param x A pointer to the start of a binary data buffer.
@return The extracted value on success, or zero on failure.
*/

#define pakReadUI32_LE(x) ((x) ?\
  ((unsigned long)(x)[0]    ) +\
  ((unsigned long)(x)[1]<<8 ) +\
  ((unsigned long)(x)[2]<<16) +\
  ((unsigned long)(x)[3]<<24) : 0)

/**
@brief Create a new archive structure.

This is called internally by the various pakLoad() functions.

@param path The name of (and path to) the archive file.
@param type The file format of the archive file.
@param count The number of entries in the archive file.
@return A new pak_s structure on success or NULL on failure.
*/

static pak_s *pakCreate(const char *path, pak_type_t type, long count)
{

  pak_s *pak  = NULL;
  int   error = 0;
  long  i     = 0;

  /* Check parameters. */

  if (!path || type == NUM_PAKTYPES || count < 1) { return NULL; }

  /* Allocate memory for the base archive structure. */

  pak = (pak_s*)malloc (sizeof(pak_s));

  if (!pak) { return NULL; }

  memset (pak, 0, sizeof(pak_s));

  /* Initialise structure. */

  pak->type  = type;
  pak->count = count;

  /* TODO: This should be a malloc()'d buffer so that longer paths work... */

  strncpy (pak->path, path, PAK_MAXFILENAME);

  /* Allocate memory for the entries. */

  if (!error)
  {

    pak->entry = (pak_item_s**)malloc (sizeof(pak_item_s*) * pak->count);

    if (pak->entry)
    {

      for (i = 0; i < pak->count; i++)
      {

        pak->entry[i] = (pak_item_s*)malloc (sizeof(pak_item_s));

        if (pak->entry[i]) { memset (pak->entry[i], 0, sizeof(pak_item_s)); }
        else               { error = 1;                                     }

      }

    } else { error = 1; }

  }

  /* If any errors occurred, free the faulty structure and return NULL. */

  if (error)
  {

    pakFree (pak);

    return NULL;

  }

  /* Return result. */

  return pak;

}

/**
@brief Update the values in an entry header.

Note that this doesn't actually CREATE a new entry header - it just updates an
existing one.

@param head The entry to be updated.
@param name The name of the entry.
@param offs The offset value of the entry.
@param size The length value of the entry.
@return Returns zero on success or non-zero on failure.
*/

static int pakInitItem(pak_item_s *head, const char *name, long offs, long size)
{

  if (!head || !name || offs < 0 || size < 0) { return -1; }

  head->offs = offs;
  head->size = size;

  strncpy(head->name, name, PAK_MAXENTRYNAME);

  return 0;

}

/**
@brief Load a Quake/Quake II or Half-Life .PAK file.

Daikatana .PAK files use a different format, and so are handled separately.

@param path The name of (and path to) the archive file.
@param len The length of the data array in bytes.
@param data A raw binary data buffer containing the archive data.
@return A new pak_s structure on success or NULL on failure.
*/

static pak_s *pakLoadQPAK(const char *path, long len, const unsigned char *data)
{
  pak_s *pak = NULL;
  unsigned long toc_offs, i;

  if      (len < PACK_QPAK_HEAD_SIZE + PACK_QPAK_INFO_SIZE) { return NULL; }
  else if (pakReadUI32_LE(data) != PACK_QPAK_4CC          ) { return NULL; }
  toc_offs = pakReadUI32_LE(&data[4]); /* Offset to header records in bytes. */
  i        = pakReadUI32_LE(&data[8]); /* Length of header records in bytes. */

  /* Try to filter out Daikatana PAK files; they have a different INFO size. */
  if      (i        % PACK_QPAK_INFO_SIZE) { return NULL; }
  else if (toc_offs < PACK_QPAK_HEAD_SIZE) { return NULL; }
  else if ((long)(toc_offs + i) > len    ) { return NULL; }

  pak = pakCreate(path, PAKTYPE_QPAK, i / PACK_QPAK_INFO_SIZE);
  if (!pak) { return NULL; }

  for (i = 0; (long)i < pak->count; ++i)
  {
    const unsigned long offs = toc_offs + (i * PACK_QPAK_INFO_SIZE);

    char name[PACK_QPAK_NAME_SIZE + 1];
    unsigned long data_offs, data_size;

    name[PACK_QPAK_NAME_SIZE] = 0;
    memcpy(name, &data[offs], PACK_QPAK_NAME_SIZE);
    data_offs = pakReadUI32_LE(&data[offs + PACK_QPAK_NAME_SIZE + 0]);
    data_size = pakReadUI32_LE(&data[offs + PACK_QPAK_NAME_SIZE + 4]);

    /* Again, try to detect cases where a Daikatana-style PAK is being used: */
    if (data_offs + data_size > /*toc_offs*/ (unsigned long)len)
    {
      pakFree(pak);
      return NULL;
    }
    /* NOTE: Some Half-Life PAK items have offset 0 if their size is also 0. */
    else if (data_offs < PACK_QPAK_HEAD_SIZE && data_size > 0)
    {
      pakFree(pak);
      return NULL;
    }

    pakInitItem(pak->entry[i], name, data_offs, data_size);
  }

  return pak;
}

/**
@brief Load a Daikatana .PAK as an archive.

@param name The name of (and path to) the archive file.
@param len The length of the data array in bytes.
@param data A raw binary data buffer containing the archive data.
@return A new pak_s structure on success or NULL on failure.
*/

static pak_s *pakLoadDPAK(const char *name, long len, const unsigned char *data)
{

  pak_s *pak = NULL;

  long cat_offs, i, fault, compression;

  if (!data || len < 84 || !name) { return NULL; }

  if (memcmp (data, "PACK", 4) != 0) { return NULL; }

  /* Get offset to header records. */

  cat_offs  = data[4];
  cat_offs += data[5] * 256;
  cat_offs += data[6] * 256 * 256;
  cat_offs += data[7] * 256 * 256 * 256;

  i  = data[8 ];
  i += data[9 ] * 256;
  i += data[10] * 256 * 256;
  i += data[11] * 256 * 256 * 256;

  if (i % 72 != 0) { return NULL; }

  if (cat_offs < 12 || cat_offs > len-i) { return NULL; }

  /* Allocate memory for main structure. */

  pak = (pak_s*)malloc (sizeof(pak_s));

  if (!pak) { return NULL; }

  /* Allocate memory for entries. */

  pak->count = i / 72;

  pak->entry = (pak_item_s**)malloc (sizeof(pak_item_s*) * pak->count);

  if (!pak->entry)
  {

    free (pak);

    return NULL;

  }

  /* Copy file name. */

  strncpy(pak->path, name, PAK_MAXFILENAME);

  pak->type = PAKTYPE_DPAK;

  /* Load header records. */

  fault = 0;

  for (i = 0; i < pak->count; i++)
  {

    pak->entry[i] = (pak_item_s*)malloc (sizeof(pak_item_s));

    if (pak->entry[i])
    {

      memset (pak->entry[i]->name, 0, PAK_MAXENTRYNAME);

      memcpy (pak->entry[i]->name, &data[cat_offs+(i*72)], 56);

      pak->entry[i]->offs  = data[cat_offs+(i*72)+56];
      pak->entry[i]->offs += data[cat_offs+(i*72)+57] * 256;
      pak->entry[i]->offs += data[cat_offs+(i*72)+58] * 256 * 256;
      pak->entry[i]->offs += data[cat_offs+(i*72)+59] * 256 * 256 * 256;

      pak->entry[i]->size  = data[cat_offs+(i*72)+60];
      pak->entry[i]->size += data[cat_offs+(i*72)+61] * 256;
      pak->entry[i]->size += data[cat_offs+(i*72)+62] * 256 * 256;
      pak->entry[i]->size += data[cat_offs+(i*72)+63] * 256 * 256 * 256;

      compression  = data[cat_offs+(i*72)+68];
      compression += data[cat_offs+(i*72)+69] * 256;
      compression += data[cat_offs+(i*72)+70] * 256 * 256;
      compression += data[cat_offs+(i*72)+71] * 256 * 256 * 256;

      /* Compressed entries can't be read yet, so set their length to zero. */

      if (compression != 0) { pak->entry[i]->size = 0; }

      if (pak->entry[i]->offs >= cat_offs &&
          pak->entry[i]->offs <  cat_offs + (pak->count * 72))    { fault = 1; }
      if (pak->entry[i]->offs < 12 || pak->entry[i]->offs >= len) { fault = 1; }
      if (pak->entry[i]->size  > (len - 12) - (pak->count * 72))   { fault = 1; }

    } else { fault = 1; }

  }

  if (fault != 0)
  {

    pakFree (pak);

    pak = NULL;

  }

  /* Return result. */

  return pak;

}

/**
@brief Load a Quake II WAD2 as an archive.

@param name The name of (and path to) the archive file.
@param len The length of the data array in bytes.
@param data A raw binary data buffer containing the archive data.
@return A new pak_s structure on success or NULL on failure.
*/

static pak_s *pakLoadWAD2(const char *name, long len, const unsigned char *data)
{

  pak_s *pak = NULL;

  long cat_offs, i, fault;

  /* Ensure that the data buffer is valid. */

  if (!data || len < 12 || !name) { return NULL; }

  if (memcmp (data, "WAD2", 4) != 0 && memcmp (data, "WAD3", 4) != 0)
  {

    return NULL;

  }

  /* Get offset to header records. */

  i  = data[4];
  i += data[5] * 256;
  i += data[6] * 256 * 256;
  i += data[7] * 256 * 256 * 256;

  cat_offs  = data[8 ];
  cat_offs += data[9 ] * 256;
  cat_offs += data[10] * 256 * 256;
  cat_offs += data[11] * 256 * 256 * 256;

  if (cat_offs < 8 || cat_offs > len-(i*32)) { return NULL; }

  /* Allocate memory for main structure. */

  pak = (pak_s*)malloc (sizeof(pak_s));

  if (!pak) { return NULL; }

  /* Allocate memory for entries. */

  pak->count = i;

  pak->entry = (pak_item_s**)malloc (sizeof(pak_item_s*) * pak->count);

  if (!pak->entry)
  {

    free (pak);

    return NULL;

  }

  /* Copy file name. */

  strncpy(pak->path, name, PAK_MAXFILENAME);

  if (data[3] == '2') { pak->type = PAKTYPE_WAD2; }
  else                { pak->type = PAKTYPE_WAD3; }

  /* Load header records. */

  fault = 0;

  for (i = 0; i < pak->count; i++)
  {

    pak->entry[i] = (pak_item_s*)malloc (sizeof(pak_item_s));

    if (pak->entry[i])
    {

      memset (pak->entry[i]->name, 0, PAK_MAXENTRYNAME);

      pak->entry[i]->offs  = data[cat_offs+(i*32)+0];
      pak->entry[i]->offs += data[cat_offs+(i*32)+1] * 256;
      pak->entry[i]->offs += data[cat_offs+(i*32)+2] * 256 * 256;
      pak->entry[i]->offs += data[cat_offs+(i*32)+3] * 256 * 256 * 256;

      pak->entry[i]->size  = data[cat_offs+(i*32)+4];
      pak->entry[i]->size += data[cat_offs+(i*32)+5] * 256;
      pak->entry[i]->size += data[cat_offs+(i*32)+6] * 256 * 256;
      pak->entry[i]->size += data[cat_offs+(i*32)+7] * 256 * 256 * 256;

      memcpy (pak->entry[i]->name, &data[cat_offs+(i*32)+16], 16);

      /* Compressed entries can't be read yet, so set their length to zero. */

      if (data[cat_offs+(i*32)+13] != 0) { pak->entry[i]->size = 0; }

    } else { fault = 1; }

  }

  if (fault != 0)
  {

    pakFree (pak);

    pak = NULL;

  }

  /* Return result. */

  return pak;

}

/**
@brief Load a DOOM WAD as an archive.

@param name The name of (and path to) the archive file.
@param len The length of the data array in bytes.
@param data A raw binary data buffer containing the archive data.
@return A new pak_s structure on success or NULL on failure.
*/

static pak_s *pakLoadWAD(const char *name, long len, const unsigned char *data)
{

  pak_s *pak = NULL;

  long cat_offs, i, fault;

  /* Ensure that the data buffer is valid. */

  if (!data || len < 12 || !name) { return NULL; }

  if (memcmp (data, "IWAD", 4) != 0 && memcmp (data, "PWAD", 4) != 0)
  {

    return NULL;

  }

  /* Get offset to header records. */

  i  = data[4];
  i += data[5] * 256;
  i += data[6] * 256 * 256;
  i += data[7] * 256 * 256 * 256;

  cat_offs  = data[8 ];
  cat_offs += data[9 ] * 256;
  cat_offs += data[10] * 256 * 256;
  cat_offs += data[11] * 256 * 256 * 256;

  if (cat_offs < 8 || cat_offs > len-(i*16)) { return NULL; }

  /* Allocate memory for main structure. */

  pak = (pak_s*)malloc (sizeof(pak_s));

  if (!pak) { return NULL; }

  /* Allocate memory for entries. */

  pak->count = i;

  pak->entry = (pak_item_s**)malloc (sizeof(pak_item_s*) * pak->count);

  if (!pak->entry)
  {

    free (pak);

    return NULL;

  }

  /* Copy file name. */

  strncpy(pak->path, name, PAK_MAXFILENAME);

  if (data[0] == 'I') { pak->type = PAKTYPE_IWAD; }
  else                { pak->type = PAKTYPE_PWAD; }

  /* Load header records. */

  fault = 0;

  for (i = 0; i < pak->count; i++)
  {

    pak->entry[i] = (pak_item_s*)malloc (sizeof(pak_item_s));

    if (pak->entry[i])
    {

      memset (pak->entry[i]->name, 0, PAK_MAXENTRYNAME);

      pak->entry[i]->offs  = data[cat_offs+(i*16)+0];
      pak->entry[i]->offs += data[cat_offs+(i*16)+1] * 256;
      pak->entry[i]->offs += data[cat_offs+(i*16)+2] * 256 * 256;
      pak->entry[i]->offs += data[cat_offs+(i*16)+3] * 256 * 256 * 256;

      pak->entry[i]->size  = data[cat_offs+(i*16)+4];
      pak->entry[i]->size += data[cat_offs+(i*16)+5] * 256;
      pak->entry[i]->size += data[cat_offs+(i*16)+6] * 256 * 256;
      pak->entry[i]->size += data[cat_offs+(i*16)+7] * 256 * 256 * 256;

      memcpy (pak->entry[i]->name, &data[cat_offs+(i*16)+8], 8);

    } else { fault = 1; }

  }

  if (fault != 0)
  {

    pakFree (pak);

    pak = NULL;

  }

  /* Return result. */

  return pak;

}

/**
@brief Load a Hyperspace Delivery Boy MPC archive.

@param name The name of (and path to) the archive file.
@param len The length of the data array in bytes.
@param data A raw binary data buffer containing the archive data.
@return A new pak_s structure on success or NULL on failure.
*/

static pak_s *pakLoadMPC(const char *name, long len, const unsigned char *data)
{

  pak_s *pak = NULL;

  long cat_offs, i, fault;

  /* Ensure that the data buffer is valid. */

  if (!data || len < 8 || !name) { return NULL; }

  if (memcmp (data, "MPCU", 4) != 0) { return NULL; }

  /* Get offset to header records. */

  cat_offs  = data[4];
  cat_offs += data[5] * 256;
  cat_offs += data[6] * 256 * 256;
  cat_offs += data[7] * 256 * 256 * 256;

  i  = data[cat_offs + 0];
  i += data[cat_offs + 1] * 256;
  i += data[cat_offs + 2] * 256 * 256;
  i += data[cat_offs + 3] * 256 * 256 * 256;

  if (cat_offs < 8 || cat_offs > len-(i*80)) { return NULL; }

  /* Allocate memory for main structure. */

  pak = (pak_s*)malloc (sizeof(pak_s));

  if (!pak) { return NULL; }

  /* Allocate memory for entries. */

  pak->count = i;

  pak->entry = (pak_item_s**)malloc (sizeof(pak_item_s*) * pak->count);

  if (!pak->entry)
  {

    free (pak);

    return NULL;

  }

  /* Copy file name. */

  strncpy(pak->path, name, PAK_MAXFILENAME);

  pak->type = PAKTYPE_MPC;

  /* Load header records. */

  fault = 0;

  for (i = 0; i < pak->count; i++)
  {

    pak->entry[i] = (pak_item_s*)malloc (sizeof(pak_item_s));

    if (pak->entry[i])
    {

      memset (pak->entry[i]->name, 0, PAK_MAXENTRYNAME);

      memcpy (pak->entry[i]->name, &data[cat_offs+(i*80)+4], 64);

      pak->entry[i]->offs  = data[cat_offs+(i*80)+68];
      pak->entry[i]->offs += data[cat_offs+(i*80)+69] * 256;
      pak->entry[i]->offs += data[cat_offs+(i*80)+70] * 256 * 256;
      pak->entry[i]->offs += data[cat_offs+(i*80)+71] * 256 * 256 * 256;

      pak->entry[i]->size  = data[cat_offs+(i*80)+72];
      pak->entry[i]->size += data[cat_offs+(i*80)+73] * 256;
      pak->entry[i]->size += data[cat_offs+(i*80)+74] * 256 * 256;
      pak->entry[i]->size += data[cat_offs+(i*80)+75] * 256 * 256 * 256;

      /* If these two values are not the same, then something is wrong... */

      if (memcmp (&data[cat_offs+(i*80)+72], &data[cat_offs+(i*80)+76], 4) != 0)
      {

        pak->entry[i]->size = 0;

      }

    } else { fault = 1; }

  }

  if (fault != 0)
  {

    pakFree (pak);

    pak = NULL;

  }

  /* Return result. */

  return pak;

}

/**
@brief Load a Descent HOG archive.

@param name The name of (and path to) the archive file.
@param len The length of the data array in bytes.
@param data A raw binary data buffer containing the archive data.
@return A new pak_s structure on success or NULL on failure.
*/

static pak_s *pakLoadHOG(const char *name, long len, const unsigned char *data)
{

  pak_s *pak = NULL;

  long offs, count, i, fault;

  /* Ensure that the data buffer is valid. */

  if (!data || len < 20 || !name) { return NULL; }

  if (memcmp (data, "DHF", 3) != 0) { return NULL; }

  /* Get entry count. */

  count = 0;
  offs  = 3;

  while (offs < len)
  {

    count++;

    i  = data[offs+13];
    i += data[offs+14] * 256;
    i += data[offs+15] * 256 * 256;
    i += data[offs+16] * 256 * 256 * 256;

    offs += 17 + i;

  }

  if (count == 0) { return NULL; }

  /* Allocate memory for main structure. */

  pak = (pak_s*)malloc (sizeof(pak_s));

  if (!pak) { return NULL; }

  /* Allocate memory for entries. */

  pak->count = count;

  pak->entry = (pak_item_s**)malloc (sizeof(pak_item_s*) * pak->count);

  if (!pak->entry)
  {

    free (pak);

    return NULL;

  }

  /* Copy file name. */

  strncpy(pak->path, name, PAK_MAXFILENAME);

  pak->type = PAKTYPE_HOG;

  /* Load header records. */

  fault = 0;
  offs  = 3;

  for (i = 0; i < pak->count; i++)
  {

    pak->entry[i] = (pak_item_s*)malloc (sizeof(pak_item_s));

    if (pak->entry[i])
    {

      memset (pak->entry[i]->name, 0, PAK_MAXENTRYNAME);

      memcpy (pak->entry[i]->name, &data[offs], 13);

      pak->entry[i]->size  = data[offs+13];
      pak->entry[i]->size += data[offs+14] * 256;
      pak->entry[i]->size += data[offs+15] * 256 * 256;
      pak->entry[i]->size += data[offs+16] * 256 * 256 * 256;

      pak->entry[i]->offs = offs + 17;

      offs += 17 + pak->entry[i]->size;

    } else { fault = 1; }

  }

  if (fault != 0)
  {

    pakFree (pak);

    pak = NULL;

  }

  /* Return result. */

  return pak;

}

/**
@brief Load a Build Engine GRP archive.

@param name The name of (and path to) the archive file.
@param len The length of the data array in bytes.
@param data A raw binary data buffer containing the archive data.
@return A new pak_s structure on success or NULL on failure.
*/

static pak_s *pakLoadGRP(const char *name, long len, const unsigned char *data)
{

  pak_s *pak = NULL;

  long offs, fault, i;

  /* Ensure that the data buffer is valid. */

  if (!data || len < 25 || !name) { return NULL; }

  if (memcmp (data, "KenSilverman", 12) != 0) { return NULL; }

  /* Get entry count. */

  i  = data[12];
  i += data[13] * 256;
  i += data[14] * 256 * 256;
  i += data[15] * 256 * 256 * 256;

  if (i == 0 || len < 16 + (i*16)) { return NULL; }

  /* Allocate memory for main structure. */

  pak = (pak_s*)malloc (sizeof(pak_s));

  if (!pak) { return NULL; }

  /* Allocate memory for entries. */

  pak->count = i;

  pak->entry = (pak_item_s**)malloc (sizeof(pak_item_s*) * pak->count);

  if (!pak->entry)
  {

    free (pak);

    return NULL;

  }

  /* Copy file name. */

  strncpy(pak->path, name, PAK_MAXFILENAME);

  pak->type = PAKTYPE_GRP;

  /* Load header records. */

  fault = 0;
  offs  = 16 + (pak->count*16);

  for (i = 0; i < pak->count; i++)
  {

    pak->entry[i] = (pak_item_s*)malloc (sizeof(pak_item_s));

    if (pak->entry[i])
    {

      memset (pak->entry[i]->name, 0, PAK_MAXENTRYNAME);

      memcpy (pak->entry[i]->name, &data[16+(i*16)], 12);

      pak->entry[i]->size  = data[16+(i*16)+12+0];
      pak->entry[i]->size += data[16+(i*16)+12+1] * 256;
      pak->entry[i]->size += data[16+(i*16)+12+2] * 256 * 256;
      pak->entry[i]->size += data[16+(i*16)+12+3] * 256 * 256 * 256;

      pak->entry[i]->offs = offs;

      offs += pak->entry[i]->size;

    } else { fault = 1; }

  }

  if (fault != 0)
  {

    pakFree (pak);

    pak = NULL;

  }

  /* Return result. */

  return pak;

}

/**
@brief Load a System Shock RES archive.

Both System Shock 1 and 2 use these, but the sequel only has 4 of them.

**Support for this format is experimental and some archives WILL NOT work.**
Specifically, it appears that some entries contain more data than described in
the entry list. This seems to happen when certain bitflags are set (flag 0x0001
looks like it could be one indicator for this - the "intro.res" file for System
Shock 1 is an example of the data block ending sooner than expected).

The "mfdfrn.res" in System Shock 1 has an example of the SizeA and SizeB values
not matching, which suggests that compression may be used in some (rare) cases.

@param path The name of (and path to) the archive file.
@param len The length of the data array in bytes.
@param data A raw binary data buffer containing the archive data.
@return A new pak_s structure on success or NULL on failure.
*/

static pak_s *pakLoadRES(const char *path, long len, const unsigned char *data)
{

  pak_s *pak = NULL;

  long head_offs = 0;
  long data_offs = 0;
  long data_size = 0;
  long offs, i;

  char name[8];

  /* Check header. */

  if (len < 134 || !data) { return NULL; }

  if (memcmp (data, "LG Res File v2\r\n", 16) != 0) { return NULL; }

  /* Skip past the unused "zeroes" section. */

  data_size = pakReadUI32_LE (&data[16]);

  for (offs = 20; offs < 20 + (data_size * 4); offs += 4)
  {

    if (pakReadUI32_LE (&data[offs]) != 0) { return NULL; }

  }

  /* Get the offset of the entry list. */

  head_offs = pakReadUI32_LE (&data[offs]);

  /* Create package structure. */

  pak = pakCreate (path, PAKTYPE_RES, pakReadUI16_LE (&data[head_offs]));

  if (!pak) { return NULL; }

  /* Loop through each entry in turn. */

  data_offs = pakReadUI32_LE (&data[head_offs + 2]);

  for (i = 0; i < pak->count; i++)
  {

    /* Entries are 10 bytes long. They contain a GUID, 2 SIZEs, and 2 FLAGs. */

    offs = head_offs + 6 + (i*10);

    /* Entries in RES files don't have names so use the GUID number instead. */

    sprintf (name, "%d", pakReadUI16_LE (&data[offs]));

    /* Each entry has two size values, so make sure that they match... */

    data_size = pakReadUI16_LE (&data[offs+6]);

    if (data_size != pakReadUI16_LE (&data[offs+2]))
    {

      /* TODO: This file appears to be compressed... */

    }

    /* Update the entry. */

    pakInitItem (pak->entry[i], name, data_offs, data_size);

    /* The offset of each entry is relative to the previous one. */

    data_offs += pak->entry[i]->size;

    while (data_offs % 4) { data_offs++; }

  }

  /* Return the result. */

  return pak;

}

/**
@brief Load a Red Faction Volition Project Pack (VPP) archive.

@todo Some Red Faction 2 .VPP archives contain compressed entries. These aren't
handled properly yet; the extracted files will contain the *compressed* data of
the VPP file, whereas we SHOULD be decompressing stuff when we extract it...

@param path The name of (and path to) the archive file.
@param len The length of the data array in bytes.
@param data A raw binary data buffer containing the archive data.
@return A new pak_s structure on success or NULL on failure.
*/

static pak_s *pakLoadVPP(const char *path, long len, const unsigned char *data)
{
  const unsigned long BLOCK_SIZE = 2048;
  unsigned long version, file_size, num_items, data_offs, head_size, i;
  pak_s *pak = NULL;

  if (len < 16 || !data || pakReadUI32_LE(data) != 0x51890ACE) { return NULL; }
  version   = pakReadUI32_LE(&data[4]);
  num_items = pakReadUI32_LE(&data[8]);
  file_size = pakReadUI32_LE(&data[12]);
  if (file_size < (unsigned long)len) { return NULL; }

  if      (version == 1) { head_size = 64; }  /* Red Faction 1. */
  else if (version == 2) { head_size = 32; }  /* Red Faction 2. */
  else                   { return NULL;    }  /* Other games..? */

  pak = pakCreate(path, version == 1 ? PAKTYPE_VPP1 : PAKTYPE_VPP2, num_items);
  if (!pak) { return NULL; }

  data_offs = BLOCK_SIZE + (pak->count * head_size);
  while (data_offs % BLOCK_SIZE) { ++data_offs; }

  for (i = 0; i < (unsigned long)pak->count; ++i)
  {
    unsigned long head_offs = BLOCK_SIZE + (i * head_size);
    unsigned long data_size = pakReadUI32_LE(&data[head_offs + head_size - 4]);
    const char *name = (const char*)&data[head_offs];

    if (version == 2)
    {
      unsigned long raw_size = pakReadUI32_LE(&data[head_offs + head_size - 8]);
      if (data_size != raw_size)
      {
        /* TODO: Figure out how the compression works. */
        fprintf(stderr, "[VPP] WARNING: %s is compressed!\n", name);
      }
    }

    pakInitItem(pak->entry[i], name, data_offs, data_size);
    for (data_offs += data_size; data_offs % BLOCK_SIZE; ++data_offs) {}
  }

  return pak;
}

/**
@brief Load a Chasm: The Rift (BIN) archive.

@param path The name of (and path to) the archive file.
@param len The length of the data array in bytes.
@param data A raw binary data buffer containing the archive data.
@return A new pak_s structure on success or NULL on failure.
*/

static pak_s *pakLoadCSM(const char *path, long len, const unsigned char *data)
{
  pak_s *pak = NULL;
  long i = 0;

  if (!path || len < 6 + 21 + 1 || !data) { return NULL; }
  if (memcmp("CSid", data, 4) != 0) { return NULL; }

  pak = pakCreate(path, PAKTYPE_CSM, pakReadUI16_LE(&data[4]));
  if (!pak) { return NULL; }

  for (i = 0; i < pak->count; ++i)
  {
    const long offs = 6 + (i * 21);

    char name[13];
    size_t name_len = data[offs];

    memset(name, 0, 13);
    memcpy(name, &data[offs + 1], name_len < 12 ? name_len : 12);

    pakInitItem(pak->entry[i], name, pakReadUI32_LE(&data[offs + 17]),
                                     pakReadUI32_LE(&data[offs + 13]));
  }

  return pak;
}

/**
@brief Load a Dark Engine DarkDB archive.

These will typically end with .GAM (Game) or .MIS (Mission).

@param path The name of (and path to) the archive file.
@param len The length of the data array in bytes.
@param data A raw binary data buffer containing the archive data.
@return A new pak_s structure on success or NULL on failure.
*/

static pak_s *pakLoadDark(const char *path, long len, const unsigned char *data)
{

  const long LIST_SIZE = 20;  /* Size of a single chunk list entry in bytes. */
  const long HEAD_SIZE = 24;  /* Size of the per-entry header data in bytes. */

  pak_s *pak = NULL;

  long list_offs;
  long offs, i;

  /* Get the offset of the chunk list and ensure that it is valid. */

  if (!path || len < 272 + LIST_SIZE || !data) { return NULL; }

  list_offs = pakReadUI32_LE (data);

  if (list_offs < 272 || list_offs > len - (4 + LIST_SIZE)) { return NULL; }

  /* Check version information. */

  if (pakReadUI32_LE (&data[ 4]) != 0) { return NULL; }
  if (pakReadUI32_LE (&data[ 8]) != 1) { return NULL; }
  if (pakReadUI32_LE (&data[12]) != 0) { return NULL; }

  /* Ensure that the following bytes are zeroes, as expected. */

  for (i = 16; i < 268; i += 4)
  {

    if (pakReadUI32_LE (&data[i]) != 0) { return NULL; }

  }

  /* Check for End-Of-File-Header signature.  */

  if      ((data[268]<<8) + data[269] != 0xDEAD) { return NULL; }
  else if ((data[270]<<8) + data[271] != 0xBEEF) { return NULL; }

  /* Create package structure. */

  pak = pakCreate (path, PAKTYPE_DARK, pakReadUI32_LE (&data[list_offs]));

  if (!pak) { return NULL; }

  /* Loop through each entry in the header. */

  for (i = 0; i < pak->count; i++)
  {

    offs = 4 + list_offs + (i * LIST_SIZE);

    pakInitItem (pak->entry[i], (const char*)&data[offs],
                 pakReadUI32_LE (&data[offs + 12]) + HEAD_SIZE,
                 pakReadUI32_LE (&data[offs + 16]));

  }

  /* Return the result. */

  return pak;

}

/**
@brief Load a Duke Nukem 2 CMP as an archive.

@param name The name of (and path to) the archive file.
@param len The length of the data array in bytes.
@param data A raw binary data buffer containing the archive data.
@return A new pak_s structure on success or NULL on failure.
*/

static pak_s *pakLoadCMP(const char *name, long len, const unsigned char *data)
{

  pak_s *pak = NULL;

  long offs, dir_count, dir_offs, expected_offs, i, j, fault;

  /* Ensure that the data buffer is valid. */

  if (!data || len < 21 || !name) { return NULL; }

  /* Verify that this is a valid CMP file. */

  dir_count     = 0;
  dir_offs      = 0;
  expected_offs = 0;
  offs          = 0;

  do {

    /* Ensure that the current offset is valid. */

    if (offs >= len) { return NULL; }

    /* Get entry offset and length. */

    i  = data[offs+12];
    i += data[offs+13] * 256;
    i += data[offs+14] * 256 * 256;
    i += data[offs+15] * 256 * 256 * 256;

    j  = data[offs+16];
    j += data[offs+17] * 256;
    j += data[offs+18] * 256 * 256;
    j += data[offs+19] * 256 * 256 * 256;

    /* An entry with an empty string and an offset/length of zero is the end. */

    if(data[offs] == 0 && i == 0 && j == 0) { break; }

    /* Check offset for validity. */

    if (i+j > len) { return NULL; }

    if (dir_count > 0)
    {

      if (expected_offs != i) { return NULL;  }
      if (dir_offs      >  i) { dir_offs = i; }

    } else { dir_offs = i; }

    /* Move to next entry. */

    expected_offs = i+j;
    dir_count++;
    offs += 20;

  } while (offs < dir_offs);

  /* Allocate memory for main structure. */

  pak = (pak_s*)malloc (sizeof(pak_s));

  if (!pak) { return NULL; }

  /* Allocate memory for entries. */

  pak->count = dir_count;

  pak->entry = (pak_item_s**)malloc (sizeof(pak_item_s*) * pak->count);

  if (!pak->entry)
  {

    free (pak);

    return NULL;

  }

  /* Copy file name. */

  strncpy(pak->path, name, PAK_MAXFILENAME);

  pak->type = PAKTYPE_CMP;

  /* Load header records. */

  fault = 0;

  for (i = 0; i < pak->count; i++)
  {

    pak->entry[i] = (pak_item_s*)malloc (sizeof(pak_item_s));

    if (pak->entry[i])
    {

      memset (pak->entry[i]->name, 0, PAK_MAXENTRYNAME);

      memcpy (pak->entry[i]->name, &data[i*20], 12);

      pak->entry[i]->offs  = data[(i*20)+12];
      pak->entry[i]->offs += data[(i*20)+13] * 256;
      pak->entry[i]->offs += data[(i*20)+14] * 256 * 256;
      pak->entry[i]->offs += data[(i*20)+15] * 256 * 256 * 256;

      pak->entry[i]->size  = data[(i*20)+16];
      pak->entry[i]->size += data[(i*20)+17] * 256;
      pak->entry[i]->size += data[(i*20)+18] * 256 * 256;
      pak->entry[i]->size += data[(i*20)+19] * 256 * 256 * 256;

    } else { fault = 1; }

  }

  if (fault != 0)
  {

    pakFree (pak);

    pak = NULL;

  }

  /* Return result. */

  return pak;

}

/*
[PUBLIC] Attempt to load an archive file.
*/

pak_s *pakLoad(const char *file_name)
{

  pak_s    *pak      = NULL;
  FILE          *pak_file = NULL;
  unsigned char *data     = NULL;
  long          len       = 0;
  int           i         = 0;
  size_t        str_len   = 0;
  char          file_ext[8];

  /* Attempt to open the specified file. */

  if (!file_name) { return NULL; }

  pak_file = fopen (file_name, "rb");

  if (!pak_file) { return NULL; }

  /* Get file length in bytes. */

  fseek (pak_file, 0, SEEK_END);
  len = ftell (pak_file);
  rewind (pak_file);

  /* Copy file data. */

  data = (unsigned char*)malloc (len);

  if (data)
  {

    /* Read data. */

    fread (data, 1, len, pak_file);

    /* Formats with file headers. */

    pak = pakLoadQPAK (file_name, len, data);

    if (!pak) { pak = pakLoadDPAK (file_name, len, data); }
    if (!pak) { pak = pakLoadWAD2 (file_name, len, data); }
    if (!pak) { pak = pakLoadWAD  (file_name, len, data); }
    if (!pak) { pak = pakLoadMPC  (file_name, len, data); }
    if (!pak) { pak = pakLoadGRP  (file_name, len, data); }
    if (!pak) { pak = pakLoadHOG  (file_name, len, data); }
    if (!pak) { pak = pakLoadVPP  (file_name, len, data); }
    if (!pak) { pak = pakLoadCSM  (file_name, len, data); }
    if (!pak) { pak = pakLoadRES  (file_name, len, data); }
    if (!pak) { pak = pakLoadDark (file_name, len, data); }

    /* Formats without file headers. */

    str_len = strlen (file_name);

    if (str_len >= 4)
    {

      /* Get file extension in upper case. */

      for (i = 0; i < 5; i++)
      {

        file_ext[i] = toupper (file_name[(str_len-4) + i]);

      }

      /* Check file extension. */

      if (strcmp (file_ext, ".CMP") == 0)
      {

        pak = pakLoadCMP (file_name, len, data);

      }

    }

    /* Free data. */

    free (data);

  }

  /* Close the file. */

  fclose (pak_file);

  /* Return the result. */

  return pak;

}
