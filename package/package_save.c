/**
@file
@brief Provides functions for exporting archive structures as files.

Copyright (C) 2015 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "package.h"

/**
@brief about

@param out about
@param value
*/

static void pakWriteUI32_LE(FILE *out, unsigned long value)
{
  if (!out) { return; }
  fputc((value>>0 ) & 0xff, out);
  fputc((value>>8 ) & 0xff, out);
  fputc((value>>16) & 0xff, out);
  fputc((value>>24) & 0xff, out);
}

/**
@brief Write a string as a fixed number of bytes, padded with zeroes if needed.

Truncated strings are counted as an "error" because they may cause problems...

@param out about
@param text about
@param size
@param term If non-zero, the final byte will be a NULL-terminator (zero).
@return The number of "errors" found, so that they may be added to a counter.
*/

static long pakWriteString(FILE *out, const char *text, size_t size, int term)
{

  size_t slen, i;

  if (!out || !text || size < 1) { return 1; }

  slen = strlen (text);

  if (term) { --size; }

  for (i = 0; i < size; i++)
  {

    if (i < slen) { fputc (text[i], out); }
    else          { fputc (0,       out); }

  }

  if (term) { fputc (0, out); }

  return (slen > size); /* <-- Was the string truncated to fit? */

}

/**
@brief Export a package as a DOOM I or DOOM II IWAD or PWAD file.

@param pak The original archive structure to use as a source.
@param file_name The name/path of the output file.
@param pwad_flag If true, export as a PWAD rather than an IWAD.
@return Zero on success, or non-zero on failure.
*/

static int pakSaveWAD(const pak_s *pak, const char *file_name, int pwad_flag)
{

  FILE       *out  = NULL;
  pak_data_s *data = NULL;
  long error, offs, i;

  /* Attempt to open the output file. */

  if (!pak || !file_name) { return -1; }

  out = fopen (file_name, "wb");

  if (!out) { return -1; }

  /* Get the offset to the Chunk List (The file header is 12 bytes long). */

  offs = 12;

  for (i = 0; i < pak->count; i++) { offs += pak->entry[i]->size; }

  /* Write the file header. */

  if (pwad_flag) { fputc ('P', out); }
  else           { fputc ('I', out); }

  fputc ('W', out); fputc ('A', out); fputc ('D', out);

  pakWriteUI32_LE (out, pak->count);
  pakWriteUI32_LE (out, offs);

  /* Output entry data. If an entry cannot be loaded, output zeroes instead. */

  error = 0;

  for (i = 0; i < pak->count; i++)
  {

    if (pak->entry[i]->size > 0)
    {

      data = pakLoadData (pak, pak->entry[i]);

      if (!data)
      {

        error++;

        for (offs = 0; offs < pak->entry[i]->size; offs++) { fputc (0, out); }

      } else { fwrite (data->data, 1, data->size, out); }

      pakFreeData (data);

    }

  }

  /* Output chunk list. */

  offs = 12;

  for (i = 0; i < pak->count; i++)
  {

    pakWriteUI32_LE (out, offs);
    pakWriteUI32_LE (out, pak->entry[i]->size);

    if (pakWriteString  (out, pak->entry[i]->name, 8, 0) != 0) { error++; }

    offs += pak->entry[i]->size;

  }

  /* Close the file and report the number of "errors" encountered. */

  fclose (out);

  return error;

}

/**
@brief Export a package as a Quake WAD2 or Half-Life WAD3 file.

@param pak The original archive structure to use as a source.
@param file_name The name/path of the output file.
@param wad3_flag If true, export as a WAD3 file rather than an WAD2.
@return Zero on success, or non-zero on failure.
*/

static int pakSaveWAD2(const pak_s *pak, const char *file_name, int wad3_flag)
{

  FILE          *out = NULL;
  pak_data_s *dat = NULL;
  long i, j, num_faults;

  /* Check parameters. */

  if (!pak || !file_name) { return -1; }

  if (strcmp(pak->path, file_name) == 0) { return 0; }

  /* Open output file. */

  out = fopen (file_name, "wb");

  if (!out) { return -1; }

  /* Write header. */

  fputc ('W', out);
  fputc ('A', out);
  fputc ('D', out);

  if (wad3_flag == 0) { fputc ('2', out); }
  else                { fputc ('3', out); }

  /* Write directory length. */

  j = 0;

  for (i = 0; i < pak->count; i++)
  {

    if (pak->entry[i]->size > 0) { j++; }

  }

  fwrite (&j, 4, 1, out);

  /* Write directory offset. */

  j = 12;

  for (i = 0; i < pak->count; i++) { j += pak->entry[i]->size; }

  fwrite (&j, 4, 1, out);

  /* Write file data. */

  num_faults = 0;

  for (i = 0; i < pak->count; i++)
  {

    if (pak->entry[i]->size > 0)
    {

      dat = pakLoadData (pak, pak->entry[i]);

      if (dat)
      {

        fwrite (dat->data, 1, pak->entry[i]->size, out);

        pakFreeData (dat);

      }
      else
      {

        for (j = 0; j < pak->entry[i]->size; j++) { fputc (0, out); }

        num_faults++;

      }

    } else { num_faults++; }

  }

  /* Write directory entries. */

  j = 12;

  for (i = 0; i < pak->count; i++)
  {

    if (pak->entry[i]->size > 0)
    {

      fwrite (&j,                  4, 1,  out);  /* Data offs. */
      fwrite (&pak->entry[i]->size, 4, 1,  out);  /* Data size. */
      fwrite (&pak->entry[i]->size, 4, 1,  out);  /* Data size. */

      fputc (0, out); /* File type. */
      fputc (0, out); /* Compression. Zero means none. */
      fputc (0, out); /* Unused. */
      fputc (0, out); /* Unused. */

      fwrite (pak->entry[i]->name, 1, 16, out);  /* File name. */

      j += pak->entry[i]->size;

    }

  }

  /* Close output file. */

  fclose (out);

  /* Return result. */

  return num_faults;

}

/**
@brief Export a package as a Quake or Daikatana PAK file.

@param pak The original archive structure to use as a source.
@param file_name The name/path of the output file.
@param daikatana If true, export as a Daikatana PAK rather than Quake 1/2.
@return Zero on success, or non-zero on failure.
*/

static int pakSavePAK(const pak_s *pak, const char *file_name, int daikatana)
{

  FILE          *out = NULL;
  pak_data_s *dat = NULL;
  long i, j, num_faults;

  /* Check parameters. */

  if (!pak || !file_name) { return -1; }

  if (strcmp(pak->path, file_name) == 0) { return 0; }

  /* Open output file. */

  out = fopen (file_name, "wb");

  if (!out) { return -1; }

  /* Write header. */

  fputc ('P', out);
  fputc ('A', out);
  fputc ('C', out);
  fputc ('K', out);

  /* Write directory offset. */

  j = 12;

  for (i = 0; i < pak->count; i++) { j += pak->entry[i]->size; }

  fwrite (&j, 4, 1, out);

  /* Write directory length. */

  j = 0;

  for (i = 0; i < pak->count; i++)
  {

    if (pak->entry[i]->size > 0) { j++; }

  }

  if (daikatana != 0) { j *= 72; }
  else                { j *= 64; }

  fwrite (&j, 4, 1, out);

  /* Write file data. */

  num_faults = 0;

  for (i = 0; i < pak->count; i++)
  {

    if (pak->entry[i]->size > 0)
    {

      dat = pakLoadData (pak, pak->entry[i]);

      if (dat)
      {

        fwrite (dat->data, 1, pak->entry[i]->size, out);

        pakFreeData (dat);

      }
      else
      {

        for (j = 0; j < pak->entry[i]->size; j++) { fputc (0, out); }

        num_faults++;

      }

    } else { num_faults++; }

  }

  /* Write directory entries. */

  j = 12;

  for (i = 0; i < pak->count; i++)
  {

    if (pak->entry[i]->size > 0)
    {

      fwrite (pak->entry[i]->name, 1, 56, out);  /* File name. */
      fwrite (&j,                  4, 1,  out);  /* Data offs. */
      fwrite (&pak->entry[i]->size, 4, 1,  out);  /* Data size. */

      if (daikatana != 0)
      {

        /* Compressed data size. */

        fwrite (&pak->entry[i]->size, 4, 1, out);

        /* Compression mode (Always use Zero: Uncompressed). */

        fputc (0, out);
        fputc (0, out);
        fputc (0, out);
        fputc (0, out);

      }

      j += pak->entry[i]->size;

    }

  }

  /* Close output file. */

  fclose (out);

  /* Return result. */

  return num_faults;

}

/**
@brief Export a package as a Hyperspace Delivery Boy MPC file.

@param pak The original archive structure to use as a source.
@param file_name The name/path of the output file.
@return Zero on success, or non-zero on failure.
*/

static int pakSaveMPC(const pak_s *pak, const char *file_name)
{

  FILE          *out = NULL;
  pak_data_s *dat = NULL;
  long i, j, num_faults;

  /* Check parameters. */

  if (!pak || !file_name) { return -1; }

  if (strcmp(pak->path, file_name) == 0) { return 0; }

  /* Open output file. */

  out = fopen (file_name, "wb");

  if (!out) { return -1; }

  /* Write header. */

  fputc ('M', out);
  fputc ('P', out);
  fputc ('C', out);
  fputc ('U', out);

  /* Write directory offset. */

  j = 8;

  for (i = 0; i < pak->count; i++) { j += pak->entry[i]->size; }

  fwrite (&j, 4, 1, out);

  /* Write file data. */

  num_faults = 0;

  for (i = 0; i < pak->count; i++)
  {

    if (pak->entry[i]->size > 0)
    {

      dat = pakLoadData (pak, pak->entry[i]);

      if (dat)
      {

        fwrite (dat->data, 1, pak->entry[i]->size, out);

        pakFreeData (dat);

      }
      else
      {

        for (j = 0; j < pak->entry[i]->size; j++) { fputc (0, out); }

        num_faults++;

      }

    } else { num_faults++; }

  }

  /* Write directory length. */

  j = 0;

  for (i = 0; i < pak->count; i++)
  {

    if (pak->entry[i]->size > 0) { j++; }

  }

  fwrite (&j, 4, 1, out);

  /* Write directory entries. */

  j = 8;

  for (i = 0; i < pak->count; i++)
  {

    if (pak->entry[i]->size > 0)
    {

      fwrite (pak->entry[i]->name, 1, 64, out);  /* File name. */
      fwrite (&j,                  4,  1, out);  /* Data offs. */
      fwrite (&pak->entry[i]->size, 4,  1, out);  /* Data size. */
      fwrite (&pak->entry[i]->size, 4,  1, out);  /* Data size. */

      fputc (1, out);  /* Filetype: 1 is general. */
      fputc (0, out);
      fputc (0, out);
      fputc (0, out);

      j += pak->entry[i]->size;

    }

  }

  /* Close output file. */

  fclose (out);

  /* Return result. */

  return num_faults;

}

/**
@brief Export a package as a Descent HOG archive.

@param pak The original archive structure to use as a source.
@param file_name The name/path of the output file.
@return Zero on success, or non-zero on failure.
*/

static int pakSaveHOG(const pak_s *pak, const char *file_name)
{

  FILE          *out = NULL;
  pak_data_s *dat = NULL;
  long i, num_faults;

  /* Check parameters. */

  if (!pak || !file_name) { return -1; }

  if (strcmp(pak->path, file_name) == 0) { return 0; }

  /* Open output file. */

  out = fopen (file_name, "wb");

  if (!out) { return -1; }

  /* Write header. */

  fputc ('D', out);
  fputc ('H', out);
  fputc ('F', out);

  /* Write directory entries. */

  num_faults = 0;

  for (i = 0; i < pak->count; i++)
  {

    dat = pakLoadData (pak, pak->entry[i]);

    if (dat)
    {

      fwrite (pak->entry[i]->name, 1, 13, out);  /* File name. */
      fwrite (&pak->entry[i]->size, 4, 1,  out);  /* Data size. */
      fwrite (dat->data, 1, pak->entry[i]->size, out);  /* File data. */

      pakFreeData (dat);

    } else { num_faults++; }

  }

  /* Close output file. */

  fclose (out);

  /* Return result. */

  return num_faults;

}

/**
@brief Export a package as a Build GRP archive.

@param pak The original archive structure to use as a source.
@param file_name The name/path of the output file.
@return Zero on success, or non-zero on failure.
*/

static int pakSaveGRP(const pak_s *pak, const char *file_name)
{

  FILE          *out = NULL;
  pak_data_s *dat = NULL;
  long i, j, num_faults;

  /* Check parameters. */

  if (!pak || !file_name) { return -1; }

  if (strcmp(pak->path, file_name) == 0) { return 0; }

  /* Open output file. */

  out = fopen (file_name, "wb");

  if (!out) { return -1; }

  /* Write header. */

  fputc ('K', out); fputc ('e', out); fputc ('n', out);
  fputc ('S', out); fputc ('i', out); fputc ('l', out);
  fputc ('v', out); fputc ('e', out); fputc ('r', out);
  fputc ('m', out); fputc ('a', out); fputc ('n', out);

  /* Write entry count. */

  j = 0;

  for (i = 0; i < pak->count; i++)
  {

    if (pak->entry[i]->size > 0) { j++; }

  }

  fwrite (&j, 4, 1, out);

  /* Write directory entries. */

  for (i = 0; i < pak->count; i++)
  {

    if (pak->entry[i]->size > 0)
    {

      fwrite (pak->entry[i]->name, 1, 12, out);  /* File name. */
      fwrite (&pak->entry[i]->size, 4, 1,  out);  /* Data size. */

    }

  }

  /* Write entry data. */

  num_faults = 0;

  for (i = 0; i < pak->count; i++)
  {

    if (pak->entry[i]->size > 0)
    {

      dat = pakLoadData (pak, pak->entry[i]);

      if (dat)
      {

        fwrite (dat->data, 1, pak->entry[i]->size, out);  /* File data. */

        pakFreeData (dat);

      }
      else
      {

        for (j = 0; j < pak->entry[i]->size; j++) { fputc (0, out); }

        num_faults++;

      }

    } else { num_faults++; }

  }


  /* Close output file. */

  fclose (out);

  /* Return result. */

  return num_faults;

}

/**
@brief Export a package as a Duke Nukem 2 CMP archive.

@param pak The original archive structure to use as a source.
@param file_name The name/path of the output file.
@return Zero on success, or non-zero on failure.
*/

static int pakSaveCMP(const pak_s *pak, const char *file_name)
{

  FILE          *out = NULL;
  pak_data_s *dat = NULL;
  long i, j, num_faults;

  /* Check parameters. */

  if (!pak || !file_name) { return -1; }

  if (strcmp(pak->path, file_name) == 0) { return 0; }

  /* Open output file. */

  out = fopen (file_name, "wb");

  if (!out) { return -1; }

  /* Write directory entries. */

  j = 4000;

  for (i = 0; i < 200; i++)
  {

    if (i < pak->count)
    {

      fwrite (pak->entry[i]->name, 1, 12, out);  /* File name. */
      fwrite (&j,                  4, 1,  out);  /* Data offset. */
      fwrite (&pak->entry[i]->size, 4, 1,  out);  /* Data length. */

      j += pak->entry[i]->size;

    }
    else
    {

      for (j = 0; j < 20; j++) { fputc (0, out); }

    }

  }

  /* Write entry data. */

  num_faults = 0;
  j          = pak->count;

  if (pak->count > 200)
  {

    num_faults = pak->count - 200;
    j          = 200;

  }

  for (i = 0; i < j; i++)
  {

    if (pak->entry[i]->size > 0)
    {

      dat = pakLoadData (pak, pak->entry[i]);

      if (dat)
      {

        fwrite (dat->data, 1, pak->entry[i]->size, out);  /* File data. */

        pakFreeData (dat);

      }
      else
      {

        for (j = 0; j < pak->entry[i]->size; j++) { fputc (0, out); }

        num_faults++;

      }

    } else { num_faults++; }

  }


  /* Close output file. */

  fclose (out);

  /* Return result. */

  return num_faults;

}

/**
@brief Export a package as a Dark Engine DB archive.

@param pak The original archive structure to use as a source.
@param file_name The name/path of the output file.
@return Zero on success, or non-zero on failure.
*/

static int pakSaveDark(const pak_s *pak, const char *file_name)
{

  FILE       *out  = NULL;
  pak_data_s *data = NULL;
  long error, offs, i;

  /* Attempt to open the output file. */

  if (!pak || !file_name) { return -1; }

  out = fopen(file_name, "wb");

  if (!out) { return -1; }

  /* Get the offset to the Chunk List (The file header is 272 bytes long). */

  offs = 272;

  for (i = 0; i < pak->count; i++) { offs += 24 + pak->entry[i]->size; }

  /* Output the file header. */

  pakWriteUI32_LE(out, offs); pakWriteUI32_LE(out, 0);  /* Offset to list. */
  pakWriteUI32_LE(out, 1   ); pakWriteUI32_LE(out, 0);  /* Version number. */

  for (i = 0; i < 63; i++) { pakWriteUI32_LE(out, 0); }  /* Byte padding... */

  fputc(0xDE, out); fputc(0xAD, out); fputc(0xBE, out); fputc(0xEF, out);

  /* Output entry data. If an entry cannot be loaded, output zeroes instead. */

  error = 0;

  for (i = 0; i < pak->count; i++)
  {

    if (pakWriteString(out, pak->entry[i]->name, 12, 1) != 0) { error++; }

    pakWriteUI32_LE(out, 0); /* Version? */
    pakWriteUI32_LE(out, 1); /* Version? */
    pakWriteUI32_LE(out, 0); /* Not set. */

    if (pak->entry[i]->size > 0)
    {

      data = pakLoadData(pak, pak->entry[i]);

      if (!data)
      {

        error++;

        for (offs = 0; offs < pak->entry[i]->size; offs++) { fputc(0, out); }

      } else { fwrite(data->data, 1, data->size, out); }

      pakFreeData(data);

    }

  }

  /* Output chunk list. */

  pakWriteUI32_LE(out, pak->count);

  offs = 272;

  for (i = 0; i < pak->count; i++)
  {

    pakWriteString(out, pak->entry[i]->name, 12, 1);
    pakWriteUI32_LE(out, offs);
    pakWriteUI32_LE(out, pak->entry[i]->size);

    offs += 24 + pak->entry[i]->size;

  }

  /* Close the file and report the number of "errors" encountered. */

  fclose(out);

  return error;

}

/**
@brief Export a package as a Red Faction VPP archive.

@param pak The original archive structure to use as a source.
@param file_name The name/path of the output file.
@param version Format version - currently, v1 and v2 are supported.
@return Zero on success, or non-zero on failure.
*/

static int pakSaveVPP(const pak_s *pak, const char *file_name, int version)
{
  if (pak || file_name || version) { /* TODO */ }
  return -1;
}

/*
[PUBLIC] Export an archive structure to a file, possibly in a different format.
*/

int pakSave(const pak_s *pak, pak_type_t fmt, const char *file_name)
{

  /* Don't allow the original file to be overwritten - it's needed for data! */
  if (strcmp(pak->path, file_name) == 0) { return 1; }

  switch (fmt)
  {
    case PAKTYPE_QPAK: return pakSavePAK(pak, file_name, 0);
    case PAKTYPE_DPAK: return pakSavePAK(pak, file_name, 1);
    case PAKTYPE_IWAD: return pakSaveWAD(pak, file_name, 0);
    case PAKTYPE_PWAD: return pakSaveWAD(pak, file_name, 1);
    case PAKTYPE_WAD2: return pakSaveWAD2(pak, file_name, 0);
    case PAKTYPE_WAD3: return pakSaveWAD2(pak, file_name, 1);
    case PAKTYPE_MPC:  return pakSaveMPC(pak, file_name);
    case PAKTYPE_GRP:  return pakSaveGRP(pak, file_name);
    case PAKTYPE_HOG:  return pakSaveHOG(pak, file_name);
    case PAKTYPE_CMP:  return pakSaveCMP(pak, file_name);
    case PAKTYPE_DARK: return pakSaveDark(pak, file_name);
    case PAKTYPE_VPP1: return pakSaveVPP(pak, file_name, 1);
    case PAKTYPE_VPP2: return pakSaveVPP(pak, file_name, 2);
    case PAKTYPE_CSM: /* Not implemented yet . */
    case PAKTYPE_RES: /* Not implemented yet - import is still experimental! */
    case NUM_PAKTYPES: default: break;
  }

  return -1;
}
