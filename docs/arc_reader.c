#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static unsigned short readUI16_LE (FILE *f) {
  return fgetc(f) + (fgetc(f) << 8);
}

static unsigned long readUI32_LE (FILE *f) {
  return fgetc(f) + (fgetc(f) << 8) + (fgetc(f) << 16) + (fgetc(f) << 24);
}

static void read_chunk (FILE *arc, unsigned long length) {
  char file_name[256];
  FILE *out;
  unsigned short compression_bits, buffer_size, buffer_index;
  unsigned long uncompressed_length, i, u;

  /* The maximum number of bits is 12 (8+4), so 4096 should be fine... */
  unsigned char buffer[4096];
  memset(buffer, 0, 4096);

  /* This isn't included in the "compressed size" value... */
  uncompressed_length = readUI32_LE(arc);

  /* ...but this IS, so subtract 2 bytes to get the "actual" data length: */
  if (length < 3) { return; }
  compression_bits = readUI16_LE(arc);
  length -= 2;

  /* The buffer should be 512 when compression-bits is 1, etc. */
  buffer_size = 256 << compression_bits;

  /* Open an output file... */
  sprintf(file_name, "%ld.dat", ftell(arc));
  out = fopen(file_name, "wb");
  if (!out) { return; }

  /* Uncompressed chunks can simply be copied directly to an output file: */
  if (!compression_bits) {
    for (i = 0; i < length; ++i) { fputc(fgetc(arc), out); }
    fclose(out);
    return;
  }

  /* Compressed chunks require special handling... */
  for (buffer_index = 0, u = 0, i = 0; i < length;) {
    unsigned char next_8_value_types, bit;

    /* We can get either literal bytes, or UI16 "instructions"... */
    next_8_value_types = fgetc(arc);
    ++i;

    for (bit = 0; bit < 8; ++bit) {
      if ((next_8_value_types >> bit) & 1) {
        /* A one indicates a literal value: */
        unsigned char value = fgetc(arc);
        ++i;

        fputc(value, out);
        ++u;
        buffer[buffer_index % buffer_size] = value;
        ++buffer_index;
      } else {
        unsigned char bytes[2];
        unsigned char data_length, j;
        signed short data_offset;

        /* A zero indicates an index/length to copy from: */
        bytes[0] = fgetc(arc);
        bytes[1] = fgetc(arc);
        i += 2;

        /* The lowest (8-X) bits of the second byte are the "length" value: */
        data_length = (bytes[1] & (0xFF >> compression_bits)) + 3;

        /* The first byte -- and X bits of the second byte -- are an offset: */
        bytes[1] = (signed char)bytes[1] >> (8 - compression_bits);
        data_offset = (signed short)(bytes[0] + (bytes[1] << 8));
        data_offset = ((256 >> compression_bits) + 2) + data_offset;
        data_offset += data_offset < 0 ? buffer_size : 0;

        /* NOTE: The last "run" might go past the end of the expected size! */
        for (j = 0; j < data_length && u < uncompressed_length; ++j) {
          unsigned char value = buffer[(data_offset + j) % buffer_size];
          fputc(value, out);
          ++u;
          buffer[buffer_index % buffer_size] = value;
          ++buffer_index;
        }
      }
    }
  }

  fclose(out);
}

static void read_toc (FILE *arc, long toc_number) {
  unsigned long item_count, toc_offset, i;

  fseek(arc, 8 + (toc_number * 8), SEEK_SET);
  item_count = readUI32_LE(arc);
  toc_offset = readUI32_LE(arc);

  for (i = 0; i < item_count; ++i) {
    unsigned long original_size, compressed_size, data_offset;

    fseek(arc, toc_offset + (i * 12), SEEK_SET);
    original_size = readUI32_LE(arc);
    compressed_size = readUI32_LE(arc);
    data_offset = readUI32_LE(arc);

    fprintf(
      stderr, "\tChunk %lu of %lu: %lu bytes (%lu compressed) @ offset %lu\n",
      i + 1, item_count, original_size, compressed_size, data_offset
    );

    fseek(arc, data_offset, SEEK_SET);
    read_chunk(arc, compressed_size);
  }
}

int main (int argc, char **argv) {
  FILE *arc;
  long length;

  arc = fopen(argv[1], "rb");
  if (!arc) { return 2; }

  fseek(arc, 0, SEEK_END);
  length = ftell(arc);
  rewind(arc);
  fprintf(stderr, "FILE: %s (%ld bytes)\n", argv[1], length);

  if (length > 9) {
    unsigned long toc_count = readUI32_LE(arc);
    unsigned long always_8 = readUI32_LE(arc);
    unsigned long i;

    if (always_8 == 8) {
      for (i = 0; i < toc_count; ++i) {
        fprintf(stderr, "Reading TOC %lu of %lu...\n", i + 1, toc_count);
        read_toc(arc, i);
      }
    } else {
      fprintf(stderr, "This does not appear to be a valid .arc file.\n");
    }
  } else {
    fprintf(stderr, "This file is too short to be a valid .arc file.\n");
  }

  fclose(arc);
  return 0;
}
