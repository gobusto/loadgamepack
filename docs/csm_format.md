UNOFFICIAL CHASM: THE RIFT BIN FILE FORMAT SPECIFICATION
========================================================

The full and shareware versions of the game store their various assets within a
file called CSM.BIN, which is structured as follows:

+ UINT32 Magic number: 0x64695343 (i.e. "CSid" when interpreted as ASCII text)
+ UINT16 Number of items within the archive.

Then, for each item:

+ UINT8  Length of the file name string.
+ CHAR12 The file name string, padded with zeroes if it is less than 12 bytes.
+ UINT32 Length of data in bytes.
+ UINT32 Offset to data in bytes.

NOTE: All integers are stored in little-endian format.
