/**
@file
@brief A simple program for extracting the .CPK archives used by Choro-Q games.

Tested with Seek & Destroy (Shin Combat Choro Q) and Penny Racers (Choro Q HG);
Road Trip Adventure (Choro Q HG 2) doesn't seem to use .CPK archives.

@todo This code basically works, but it needs tidying up.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CPK_4CC 0x40000000  /**< @brief CPK files all start with this value. */
#define CPK_BLOCK_SIZE 2048 /**< @brief CPK files are split into blocks.     */

static unsigned short read_ui16(FILE *src)
{
  unsigned short i = (unsigned short)fgetc(src);
  return i + ((unsigned short)fgetc(src) << 8);
}

static unsigned long read_ui32(FILE *src)
{
  unsigned long i = (unsigned long)fgetc(src);
  i += (unsigned long)fgetc(src) << 8;
  i += (unsigned long)fgetc(src) << 16;
  return i + ((unsigned long)fgetc(src) << 24);
}

int main(int argc, char **argv)
{
  FILE *src = NULL;
  long flen;
  unsigned long magic_number, num_entries, i, this_offs;

  if (argc < 2)
  {
    fprintf(stderr, "Usage: %s filename.cpk\n", argv[0]);
    return 1;
  }

  src = fopen(argv[1], "rb");
  if (!src)
  {
    fprintf(stderr, "Couldn't open %s\n", argv[1]);
    return 1;
  }

  fseek(src, 0, SEEK_END);
  flen = ftell(src);
  rewind(src);

  fprintf(stderr, "%s (%ld bytes)\n", argv[1], flen);
  if (flen < CPK_BLOCK_SIZE || flen % CPK_BLOCK_SIZE)
  {
    fprintf(stderr, "Invalid file length\n");
    goto CLEANUP;
  }

  /* The first 8 bytes are  */
  magic_number = read_ui32(src);
  num_entries = read_ui32(src);
  fprintf(stderr, "Magic Number: %08lx\n", magic_number);
  fprintf(stderr, "Num. Entries: %lu\n", num_entries);

  if (magic_number != CPK_4CC)
  {
    fprintf(stderr, "Magic Number mismatch.\n");
    goto CLEANUP;
  }

  this_offs = read_ui16(src) * CPK_BLOCK_SIZE;
  for (i = 0; i < num_entries; ++i)
  {
    unsigned long next_offs = read_ui16(src) * CPK_BLOCK_SIZE;
    unsigned long this_size = next_offs - this_offs;
    fprintf(stderr, "\tEntry %ld:\tOffs: %lu\tSize: %lu\n", i, this_offs, this_size);

    if (this_size > 0)
    {
      char name[64];
      FILE *out;

      sprintf(name, "entry%lu.bin", i);
      out = fopen(name, "wb");

      if (out)
      {
        long oldpos = ftell(src);
        fseek(src, this_offs, SEEK_SET);
        for (; this_size > 0; --this_size) { fputc(fgetc(src), out); }
        fseek(src, oldpos, SEEK_SET);
        fclose(out);
      }

    }

    this_offs = next_offs;
  }

  /* The final offset should point to the EOF: */
  if (this_offs != flen)
  {
    fprintf(stderr, "WARNING: The EOF offset doesn't match the file size!\n");
  }

  /* Close the file and end the program. */
  CLEANUP:
  fclose(src);
  return 0;
}
