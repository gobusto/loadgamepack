Terra Incognita International .ARC file format
==============================================

Some notes on how the .ARC files used by Terra Incognita International work.

Note: The *original* Terra Incognita (and Fatal Fantasy VII) work differently.

Preface: Where to get Terra Incognita International
---------------------------------------------------

Before you can extract data from the .ARC files, you'll need to obtain them:

- Yaroze version: <https://archive.org/details/terra-incognita>
- Windows version: <https://archive.org/details/terrapc>

The PSP version can be obtained via the Wayback Machine:

<https://web.archive.org/web/20050716001156/http://www.playoffline.com>

Unfortunately, the PS2 version does not seem to have been archived anywhere.

General
-------

The 3 `data#.arc` files used by Terra Incognita International start like this:

```
data1.arc: 1 8 | 31 16
data2.arc: 6 8 | 60 56 | 71 31500 | 8 71576  | 42 74384 | 26 136148 | 38 213356
data3.arc: 3 8 | 1  32 | 1  540   | 2 107184
```

These are all (little endian) UI32 values:

- The 1st UI32 is the number of "folders" to follow.
- The 2nd UI32 is always 8; perhaps "offset to" (or "length of each") folder?

This is followed by a list of "folders" in the following format:

- Number of files in this folder.
- Offset to the list of files.

Each item in those file lists has the following format:

- UI32 Original file size
- UI32 Compressed file size (excluding any "padding" bytes between each file)
- UI32 Offset to the compressed file data (always aligned to 4 bytes)

The first UI32 at that offset *also* states the uncompressed size. This doesn't
count as part of the "compressed data" length, though.

Reading (compressed) files
--------------------------

After the 4-byte "original size" for this item, there is a 16-bit integer which
seems to indicate how its contents are compressed. Note that this DOES count as
part of the "compressed size", unlike the earlier UI32, so the "real" data will
be two bytes smaller than is stated in the "list" described earlier.

If this value is zero, the chunk is uncompressed, and can simply be used as-is.
In this case, the "compressed" size will be two bytes LARGER than the original,
since (as mentioned above) the 16-bit "compression type" value counts as a part
of that "compressed" length.

If it is non-zero, then the data is read as follows:

- Read a byte. The individual bits define how the next 8 values are to be read.
- If the bit is 1, the next byte is a literal value.
- If the bit is zero, the next two bytes are an encoded value.

The bits are "in order" -- that is, 1-bit = first byte, and 128-bit = 8th byte.

Reading encoded values
----------------------

The compression number may be 1, 2, 3 or 4. Let's call this X for brevity.

Compression seems to be LZSS-based. A "recent-data" buffer will need allocating
as follows:

- If X is 1, the buffer needs to be exactly 512 bytes long.
- If X is 2, the buffer needs to be exactly 1024 bytes long.
- If X is 3, the buffer needs to be exactly 2048 bytes long.
- If X is 4, the buffer needs to be exactly 4096 bytes long.

Note that this is a "ring" buffer; bytes are added into it until it's full, and
then new bytes are added into slots 0, 1, 2, etc. (overwriting the old values).
Also note that the buffer should be filled with zeroes initially, as some files
rely on that.

Literal (unencoded) values should be added into the buffer, but they don't need
to read anything back from it.

Encoded values will both read data from this buffer and add (decompressed) data
into it. For each two-byte pair:

- The first byte, and the highest X bits of the second byte, are the offset.
- The lowest 8-X bits of the second byte are a "length" value (minus 3).

This means that the "number of bytes to output" is always 3 + (lowest 7 bits of
the second byte) if the compression type is 1. This makes sense, since anything
less than 3 repetitions would just be stored as two literal bytes instead.

The offset is a signed, twos-complement value. The first (low) byte can be used
as-is, but the 1-4 bits in the second byte will require sign extension (that
is, left-padded with zeroes or ones, based on the highest bit) before it can be
used as the "high" byte of the full, 16-bit number.

For some reason, the offset then needs "adjusting" as follows:

    offset = ((256 >> X) + 2) + offset;
    if (offset < 0) { offset += buffer_size; }

I'm not sure why this is -- perhaps there's some simplification I've overlooked
when decoding the offset bits -- but the above does at least work.

The offset points to a specific point in the "recent data" buffer. The "length"
is both the number of bytes to read from it, and the number of bytes to output.

Note that some files rely on being able to read back bytes that were added into
the buffer as part of the currently-running "copy loop", so make sure to update
the buffer IMMEDIATELY (i.e. on a byte-by-byte basis) to account for that.

Also, note that the final "run" may output more bytes than necessary, so ensure
that this is accounted for if you don't want any extra "junk" bytes at the end.

What do the .ARC files contain?
-------------------------------

`DATA1.ARC` contains the maps. The format of these is explained below.

`DATA1.ARC` also contains several 4-byte "junk" files, all of which contain the
same sequence: `32, 32, 32, 26`. I don't know what these are for.

`DATA2.ARC` contains all images, models, and animations:

- Images use the standard Playstation .TIM format.
- Models use the standard Playstation .HMD format.
- Animations seem to use a custom format, which is explained below.

`DATA3.ARC` contains two SEQ files, plus a VH and VB (VAB) pair -- all of which
are standard Playstation formats, much like .TIM or .HMD.

The VB file contains the sound effects. The PSP version simply includes them as
separate WAV files, though, so you don't really need to bother extracting them.
The PC port also uses WAVs, but they are embedded into `terra.exe` via `.rsrc`.
This means that `DATA3.ARC` is unused on those versions, but for some reason it
is included anyway.

The SEQ files aren't the in-game music; one is the "Team Fatal Presents" theme,
and the other is the "use potion" sound effect. Try converting them into MIDIs!

The actual music (and associated .VH/.VB files) is from the Net Yaroze devkit:

> At startup, `STD0.VH`, `STD0.VB`, `MUSI.SEQ` and `GOGO.SEQ` are read from the
> Net Yaroze startup disk.
>
> (Translated from <http://www.playoffline.com/yaroze> via the Wayback Machine)

This CD can be found on archive.org if you need it; look in `PSX/DATA/SOUND/`.

Everything else, such as message text, appears to be hard-coded in `TERRA.EXE`.

The map format
--------------

Each map has the following format:

- UI32 Width in cells
- UI32 Height in cells
- UI32 Depth in cells
- WxDxH "cell" values. Each cell is 4 bits, so there are two cells per byte.
- Possibly some padding bytes, if the length of the data is not divisible by 4.

A value of zero means the cell is empty. The other values vary depending on the
the "theme" of the map -- "outside" or "ruins".

For some reason, "interactive" features (such as chests or signs) aren't part of
these map files -- only "static" features, such as walls, trees, or water are.

The animation format
--------------------

Each animation file starts with the following 8-byte header:

- UI16 Frame count
- UI16 Bone count
- UI32 Offset to data (always 8)

This is followed by a list of bone data, which uses a similar format:

- UI16 Bone ID
- UI16 Keyframe count
- UI32 Offset to keyframe data

Each "keyframe" is 20 bytes long, and *seems* to work as follows:

- UI16 Frame ID (ranging from zero to frame-count-minus-one)
- UI16 Offset X
- UI16 Offset Y
- UI16 Offset Z
- UI16 Scale(?) X
- UI16 Scale(?) Y
- UI16 Scale(?) Z
- UI16 Rotation X
- UI16 Rotation Y
- UI16 Rotation Z

This has only been partially-researched, though, so the above may be incorrect.
