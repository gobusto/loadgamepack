/**
@file
@brief A simple program for extracting the .EBD archives used by Robot Warlords
*/

#include <stdio.h>
#include <stdlib.h>

#define EBD_4CC 0x000020F0
#define EBD_HEAD_SIZE 16

static unsigned long read_ui32(FILE *src)
{
  unsigned long i = (unsigned long)fgetc(src);
  i += (unsigned long)fgetc(src) << 8;
  i += (unsigned long)fgetc(src) << 16;
  i += (unsigned long)fgetc(src) << 24;
  return i;
}

int main(int argc, char **argv)
{
  FILE *src;
  long length;
  unsigned long list_size, num_items, i;

  if (argc < 2 || argc > 3) return fprintf(stderr,
    "Usage: %s filename.ebd [x]\n(HINT: Nothing is extracted unless 'x' is used)\n",
  argv[0]);

  src = fopen(argv[1], "rb");
  if (!src) { return fprintf(stderr, "Could not open file.\n"); }

  fseek(src, 0, SEEK_END);
  length = ftell(src);
  rewind(src);

  fprintf(stdout, "NAME: %s\nSIZE: %ld bytes)\n", argv[1], length);
  if (length < EBD_HEAD_SIZE + 64) { fprintf(stderr, "File is too short.\n"); goto CLEANUP; }

  /* The first 16 bytes are mainly zeroes, but the first two are `240` then `32`: */
  if (read_ui32(src) != EBD_4CC) { fprintf(stderr, "Invalid 4CC\n"); goto CLEANUP; }
  else if (read_ui32(src) !=  0) { fprintf(stderr, "Expected 0\n" ); goto CLEANUP; }
  else if (read_ui32(src) !=  0) { fprintf(stderr, "Expected 0\n" ); goto CLEANUP; }
  else if (read_ui32(src) !=  0) { fprintf(stderr, "Expected 0\n" ); goto CLEANUP; }

  /*
  The list of entries is preceded by a length value and an item count. The total
  size of the list (all entries plus these two UI32 values) should be a multiple
  of 16, so some files include extra zeroes after the last item as padding.
  */

  list_size = read_ui32(src);
  num_items = read_ui32(src);
  fprintf(stdout, "Table of Contents: %lu items (%lu bytes)\n", num_items, list_size);

  /* Each item within the list is 28 bytes in size, with no padding between: */
  for (i = 0; i < num_items; ++i)
  {
    char name[13];
    unsigned long flags, local_offset, basic_length, padded_length;

    fread(name, 1, 12, src); name[12] = 0;
    flags = read_ui32(src);         /* Not sure how these work at the moment. */
    local_offset = read_ui32(src);  /* NOTE: See below to see how this works. */
    basic_length = read_ui32(src);  /* Data length WITHOUT any extra padding. */
    padded_length = read_ui32(src); /* Length used (it's padded to 16-bytes). */

    fprintf(stdout, "\t%lu:\t%s (Flags: 0x%08lx) |", i, name, flags);
    fprintf(stdout, " Offset: %lu |", local_offset);
    fprintf(stdout, " Length: %lu |", basic_length);
    fprintf(stdout, " Padded: %lu\n", padded_length);

    /* If the user has specified `x`, actually extract the file from the EBD: */
    if (argc >= 3 && argv[2][0] == 'x' && !argv[2][1])
    {
      FILE *out = fopen(name, "wb");
      if (out)
      {
        long old_pos = ftell(src);
        unsigned long a;

        /* The actual offset needs to account for the header + entry list: */
        fseek(src, EBD_HEAD_SIZE + list_size + local_offset, SEEK_SET);
        for (a = 0; a < basic_length; ++a) { fputc(fgetc(src), out); }
        fseek(src, old_pos, SEEK_SET);

        fclose(out);
      }
    }
  }

  CLEANUP:
  fclose(src);
  return 0;
}
