/**
@file
@brief A program to extract the `data/trainmdl.dat` file from X-treme Express.

This was built by reverse-engineering the file from the PAL (SLES-50998) CD.
*/

#include <stdio.h>
#include <stdlib.h>

signed long read_si32(FILE *src)
{
  signed long i = (signed long)fgetc(src);
  i += (signed long)fgetc(src) << 8;
  i += (signed long)fgetc(src) << 16;
  i += (signed char)fgetc(src) << 24;
  return i;
}

int main(int argc, char **argv)
{
  FILE *src;
  long length;
  int i;

  if (argc != 2)
  {
    fprintf(stderr, "Usage: %s path/to/trainmdl.dat\n", argv[0]);
    return 1;
  }

  src = fopen(argv[1], "rb");
  if (!src)
  {
    fprintf(stderr, "Could not open %s\n", argv[1]);
    return 2;
  }

  fseek(src, 0, SEEK_END);
  length = ftell(src);
  rewind(src);

  /* A file in this format could be as small as a 64-byte header + 1 item... */
  if (length < 128)
  {
    fprintf(stderr, "File is too short\n");
    goto CLEANUP;
  }

  /* Ensure that this is actually a Train Model pack file: */
  for (i = 0; i < 16; ++i)
  {
    /* The first 16 bytes are a 14-byte ASCII string, padded with two zeros: */
    int expected = (i < 14) ? "TRAINMODELPACK"[i] : 0;
    if (fgetc(src) != expected)
    {
      fprintf(stderr, "Invalid header\n");
      goto CLEANUP;
    }
  }

  /* Next comes the "table of contents" for this archive: */
  for (i = 0; ftell(src) < length; ++i)
  {
    /*
    The difference between each offset is a multiple of 64; data is padded with
    zeroes if it doesn't happen to be cleanly divisible by 64, just as the list
    itself is.
    */

    long data_size = read_si32(src);
    long data_offs = read_si32(src);

    /*
    Since there is no explicit "number of items" value, an all-zeroes "item" is
    the only indicator that there is no more data. For reference, the file used
    by SLES-50998 should contain 160 items.
    */

    if (data_offs == 0) { break; }

    /* If we've got something useful, report it: */
    fprintf(stdout, "Item #%d (%ld bytes)\n", i, data_size);

    /* Change this to enable/disable the actual "extraction" bit: */
    if (1)
    {
      FILE *out;
      char name[64];

      sprintf(name, "item%d.bin", i);
      out = fopen(name, "wb");

      if (out)
      {
        long prev_offs, a;

        prev_offs = ftell(src);
        fseek(src, data_offs, SEEK_SET);
        for (a = 0; a < data_size; ++a) { fputc(fgetc(src), out); }
        fseek(src, prev_offs, SEEK_SET);
        fclose(out);
      }
      else { fprintf(stdout, "WARNING: Could not extract this file!\n"); }
    }
  }

  /* Close the input file and end the program: */
  CLEANUP:
  fclose(src);
  return 0;
}
