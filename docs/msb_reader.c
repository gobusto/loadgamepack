/**
@file
@brief A simple program for extracting the .MSH archives of Smarties Meltdown.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static unsigned long read_ui32(FILE *src)
{
  unsigned long i = (unsigned long)fgetc(src);
  i += (unsigned long)fgetc(src) << 8;
  i += (unsigned long)fgetc(src) << 16;
  return i + ((unsigned long)fgetc(src) << 24);
}

static void extract(
  const char *file_name,
  unsigned long data_offset,
  unsigned long data_length,
  unsigned long sound_id,
  unsigned long frequency
) {
  char output_name[256];
  FILE *src;
  FILE *dst;

  src = fopen(file_name, "rb");
  if (!src) { return; }
  /* TODO: Verify that the specified offset/length are valid here! */
  fseek(src, data_offset, SEEK_SET);

  sprintf(output_name, "sound-%lu_%lu.raw", sound_id, frequency);
  dst = fopen(output_name, "wb");
  if (dst) {
    unsigned long i;
    for (i = 0; i < data_length; ++i) { fputc(fgetc(src), dst); }
    fclose(dst);
  }

  fclose(src);
}

int main(int argc, char **argv)
{
  FILE *src = NULL;
  unsigned long actual_length, expected_length, padding_length, sound_count;

  src = fopen(argv[1], "rb");
  if (!src) { return -1; }

  fseek(src, 0, SEEK_END);
  actual_length = ftell(src);
  rewind(src);
  fprintf(stderr, "LENGTH (ACTUAL): %lu\n", actual_length);
  if (actual_length < 16) { goto STOP_READING; }

  /* The first 4 bytes tell us how long the file should be: */
  expected_length = read_ui32(src);
  fprintf(stderr, "LENGTH (EXPECTED): %lu\n", expected_length);
  if (actual_length != expected_length) { goto STOP_READING; }
  if (actual_length % 4) { goto STOP_READING; }

  /* The next 4 bytes tell us how many zero bytes to ignore at the end: */
  padding_length = read_ui32(src);
  fprintf(stderr, "We are expecting %lu padding bytes\n", padding_length);

  /* This is followed by the number of entries (each of which is 4*4 bytes): */
  sound_count = read_ui32(src);
  fprintf(stderr, "We are expecting %lu sound records\n", sound_count);

  /* If all goes well, we can read the data from the sound list now: */
  if (12 + (sound_count * 4 * 4) + padding_length == expected_length) {
    unsigned long i;

    for (i = 0; i < sound_count; ++i) {
      unsigned long data_length, sound_id, data_offset, frequency;
      data_length = read_ui32(src);
      sound_id = read_ui32(src); /* I'm pretty sure that this isn't flags... */
      data_offset = read_ui32(src);
      frequency = read_ui32(src);

      fprintf(stderr, "Sound %lu:\n", i + 1);
      fprintf(stderr, "\tID: %lu\n", sound_id);
      fprintf(stderr, "\tOffset: %lu\n", data_offset);
      fprintf(stderr, "\tLength: %lu\n", data_length);
      fprintf(stderr, "\tFrequency: %lu\n", frequency);

      if (argc >= 3) {
        extract(argv[2], data_offset, data_length, sound_id, frequency);
      }
    }
  }

  STOP_READING:
  fclose(src);
  return 0;
}
