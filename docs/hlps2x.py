#!/usr/bin/env python3
"""
Half-Life (PS2) .PAK decompression utility
------------------------------------------

The .PAK files used by the Playstation version of Half Life are compressed with
ZLIB. This Python script decompresses them and writes the result as a new file,
thus converting them into standard (PC-style) .PAK files instead.
"""

import sys
import zlib

if len(sys.argv) != 3:
    print("Usage: hlps2x input.pak output.pak")
    exit(1)
# Open the input .PAK file and decompress it:
with open(sys.argv[1], "rb") as src:
    expected_len = str(int.from_bytes(src.read(4), byteorder="little"))
    data = zlib.decompress(src.read())
# Warn the user if the data size differs from what we expected:
actual_len = str(len(data))
if expected_len != actual_len:
    print(" ".join(["Expected", expected_len, "bytes, but got", actual_len]))
# Write the resulting (decompressed) data to another file:
with open(sys.argv[2], "wb") as out:
    out.write(data)
