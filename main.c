/**
@file
@brief A simple archive extractor to demonstrate the archive-handling code.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "package/package.h"

/**
@brief This is where the program begins.

@param argc Length of the argv array.
@param argv Program arguments.
@return Zero on success or non-zero on failure.
*/

int main(int argc, char *argv[])
{
  int a;
  for (a = 1; a < argc; ++a)
  {
    pak_s *pack = pakLoad(argv[a]);
    if (pack)
    {
      long i;
      fprintf(stderr, "Extracting %s...\n", argv[a]);
      for (i = 0; i < pack->count; ++i)
      {
        pak_data_s *data = pakLoadData(pack, pack->entry[i]);
        if (data)
        {
          char cmd[9 + PAK_MAXENTRYNAME];
          size_t len;

          sprintf(cmd, "mkdir -p %s", pack->entry[i]->name);
          for (len = strlen(cmd); len > 0; --len)
          {
            if (cmd[len-1] == '/' || cmd[len-1] == '\\')
            {
              cmd[len-1] = 0;
              system(cmd);
              break;
            }
          }

          if (pakSaveData(data, pack->entry[i]->name) != 0)
            fprintf(stderr, "[FAIL] Could not save %s\n", pack->entry[i]->name);
          else
            fprintf(stderr, "[OKAY] %s\n", pack->entry[i]->name);
          pakFreeData(data);
        }
        else fprintf(stderr, "[FAIL] Couldn't load %s\n", pack->entry[i]->name);
      }
      pakFree(pack);
    }
    else { fprintf(stderr, "Could not load %s\n", argv[a]); }
  }
  return 0;
}
