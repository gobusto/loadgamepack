Generic game archive loader
===========================

Allows files to be extracted from various game engine archive formats.

Supported Formats
-----------------

The following formats can be imported, and most can also be exported as well:

+ Quake/Quake II and Half-Life .PAK
+ Daikatana .PAK (Uncompressed items only)
+ DOOM and DOOM II IWAD/PWAD
+ Quake WAD2 and Half-Life WAD3
+ Hyperspace Delivery Boy .MPC (Uncompressed items only)
+ Build Engine .GRP
+ Descent .HOG
+ Duke Nukem 2 .CMP
+ Red Faction and Red Faction 2 .VPP (Uncompressed items only)
+ Chasm: The Rift .BIN
+ System Shock and System Shock 2 .RES
+ Thief, Thief 2, and System Shock 2 DarkDB (i.e .GAM and .MIS files)

More formats may be added in the future.

Licensing
---------

This code is made available under the [MIT](http://opensource.org/licenses/MIT)
license, which allows you to use or modify it for any purpose including paid-for
and closed-source projects. All I ask in return is that proper attribution is
given (i.e. don't remove my name from the copyright text in the source code, and
perhaps include me on the "credits" screen, if your program has such a thing).
